package com.tedtalks.rss.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.util.Log;

import com.tedtalks.rss.api.RetrofitHelper;
import com.tedtalks.rss.model.RssEntity;
import com.tedtalks.rss.providers.TalksContentProvider;
import com.tedtalks.rss.utils.XmlParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 9/6/2015.
 */
public class WorkerService extends IntentService {

    public final static String ACTION = "action";
    public final static String GET_RSS = "get_rss";

    public WorkerService() {
        super("");

    }

    public WorkerService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getStringExtra(ACTION);
        if(GET_RSS.equals(action)) {
            RetrofitHelper.getApiTed().getRss(new Callback<Response>() {
                @Override
                public void success(final Response response, Response response2) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                List<RssEntity> list = XmlParser.getInstance().parse(response.getBody().in());

                                ContentValues[] cvs = new ContentValues[list.size()];
                                for(int i=0; i<list.size(); i++) {
                                    cvs[i] = list.get(i).toContentValues();
                                }

                                getContentResolver().bulkInsert(TalksContentProvider.URI_TALKS, cvs);

                                Log.d("ted", list.size() + " list size");
                            } catch (XmlPullParserException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }


}
