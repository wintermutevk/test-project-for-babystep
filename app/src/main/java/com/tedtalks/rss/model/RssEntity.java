package com.tedtalks.rss.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.tedtalks.rss.providers.TalksContentProvider;

/**
 * Created by user on 9/6/2015.
 */
public class RssEntity {

    private String title;
    private String description;
    private String urlThumbnail;
    private String urlVideo;

    public RssEntity() {
    }

    public RssEntity(Cursor cursor) {
        this.title = cursor.getString(cursor.getColumnIndex(TalksContentProvider.TITLE));
        this.description = cursor.getString(cursor.getColumnIndex(TalksContentProvider.DESCRIPTION));
        this.urlVideo = cursor.getString(cursor.getColumnIndex(TalksContentProvider.URL_VIDEO));
        this.urlThumbnail = cursor.getString(cursor.getColumnIndex(TalksContentProvider.URL_THUMBNAIL));
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TalksContentProvider.TITLE, this.title);
        contentValues.put(TalksContentProvider.DESCRIPTION, this.description);
        contentValues.put(TalksContentProvider.URL_THUMBNAIL, this.urlThumbnail);
        contentValues.put(TalksContentProvider.URL_VIDEO, this.urlVideo);
        return contentValues;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }
}
