package com.tedtalks.rss.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;

import java.sql.SQLException;

/**
 * Created by user on 9/6/2015.
 */
public class TalksContentProvider extends ContentProvider {

    private static final String DB_NAME = "usersdb";
    private static final int DB_VERSION = 2;
    public static final String TABLE_USERS = "table_users";

    public static final String ID = "_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String URL_VIDEO = "url_video";
    public static final String URL_THUMBNAIL = "url_thumbnail";


    private final static String CREATE_TABLE = "create table " + TABLE_USERS + " ("
            + ID + " integer primary key, "
            + TITLE + " title, "
            + DESCRIPTION + " text, "
            + URL_VIDEO + " text, "
            + URL_THUMBNAIL + " text "
            + ");";

    public final static String AUTHORITY = "com.testproject.avito.providers.TalksContentProvider";

    public final static Uri URI_TALKS = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USERS);

    private final static String DROP_TABLE = "drop table if exists " + TABLE_USERS;

    private final static int MATCH_USER_ITEM = 0;
    private final static int MATCH_USER_DIR = 1;
    private final static int MATCH_USER_OFFSET = 2;

    private UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH) {{
        addURI(AUTHORITY, TABLE_USERS + "/#", MATCH_USER_ITEM);
        addURI(AUTHORITY, TABLE_USERS, MATCH_USER_DIR);
        addURI(AUTHORITY, TABLE_USERS + "/offset/#", MATCH_USER_OFFSET);
    }};

    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        dbOpenHelper = new DbOpenHelper(getContext());
        return true;
    }

    private Cursor queryWithOffset(Uri uri) {
        String offset = uri.getLastPathSegment();
        db = dbOpenHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS + " LIMIT 1 OFFSET " + offset + ";",null);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        switch (URI_MATCHER.match(uri)) {
            case MATCH_USER_OFFSET:
                return queryWithOffset(uri);
            case MATCH_USER_DIR:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ID + " ASC";
                }
                break;
            case MATCH_USER_ITEM:
                selection = ID + "=" + uri.getLastPathSegment();
        }
        db = dbOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (URI_MATCHER.match(uri)) {
            case MATCH_USER_ITEM:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
        }
        db = dbOpenHelper.getWritableDatabase();
        long rowId = db.insert(TABLE_USERS, null, values);
        Uri insertUri = ContentUris.withAppendedId(uri, rowId);
        getContext().getContentResolver().notifyChange(insertUri, null);
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        db = dbOpenHelper.getWritableDatabase();
        int affectedRowCount = db.delete(TABLE_USERS, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return affectedRowCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (URI_MATCHER.match(uri)) {
            case MATCH_USER_DIR:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
            case MATCH_USER_ITEM:
                selection = ID + "=" + uri.getLastPathSegment();
                break;
        }
        db = dbOpenHelper.getWritableDatabase();
        int rowsAffected = db.update(TABLE_USERS, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case MATCH_USER_DIR:
                table = TABLE_USERS;
                break;
            default:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
        }
        db = dbOpenHelper.getWritableDatabase();
        db.beginTransaction();

        try {
            for(ContentValues cv : values) {
                long newId = db.insertOrThrow(table, null, cv);
                if(newId <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            db.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return values.length;
    }

    private class DbOpenHelper extends SQLiteOpenHelper {

        public DbOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }
    }
}
