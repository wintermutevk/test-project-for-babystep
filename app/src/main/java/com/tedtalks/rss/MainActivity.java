package com.tedtalks.rss;

import android.app.Activity;
import android.os.Bundle;

import com.tedtalks.rss.fragments.TalksListFragment;
import com.tedtalks.rss.providers.TalksContentProvider;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getContentResolver().delete(TalksContentProvider.URI_TALKS, null, null);
        getFragmentManager().beginTransaction().add(R.id.llFragmentContainer, new TalksListFragment()).commit();
    }

}
