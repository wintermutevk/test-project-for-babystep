package com.tedtalks.rss.api;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Streaming;

/**
 * Created by user on 10/6/2015.
 */
public interface ApiDownload {

    @GET("/talks/{file}?apikey=TEDRSS")
    @Streaming
    public Response getAdvertisement(@Path("file") String file);
}
