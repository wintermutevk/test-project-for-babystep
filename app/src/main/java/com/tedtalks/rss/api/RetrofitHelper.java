package com.tedtalks.rss.api;

import android.util.Log;

import retrofit.RestAdapter;

/**
 * Created by user on 9/6/2015.
 */
public class RetrofitHelper {

    private static final String URL_RSS = "https://www.ted.com";
    private static final String URL_DOWNLOAD = "http://download.ted.com";

    private static ApiTed apiTed;
    private static ApiDownload apiDownload;

    public static ApiTed getApiTed() {
        if(apiTed == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(URL_RSS)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String message) {
                            Log.d("api_log", message);
                        }
                    })
                    .build();
            apiTed = restAdapter.create(ApiTed.class);
        }
        return apiTed;
    }

    public static ApiDownload getApiDownload() {
        if(apiDownload == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(URL_DOWNLOAD)
                    .build();
            apiDownload = restAdapter.create(ApiDownload.class);
        }
        return apiDownload;
    }
}
