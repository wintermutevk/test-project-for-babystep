package com.tedtalks.rss.api;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Streaming;

/**
 * Created by user on 9/6/2015.
 */
public interface ApiTed {

    @GET("/talks/rss")
    public void getRss(Callback<Response> callback);

    @GET("{videoUrl}")
    @Streaming
    public Response getVideo(@Path("videoUrl") String videoUrl);


}
