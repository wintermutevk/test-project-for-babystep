package com.tedtalks.rss.utils;

import com.tedtalks.rss.model.RssEntity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/6/2015.
 */
public class XmlParser {

    private static final String TITLE = "title";
    private static final String DESCRIPTION ="description";
    private static final String VIDEO = "media:content";
    private static final String THUMBNAIL = "media:thumbnail";

    private static XmlParser xmlParser;

    private XmlPullParser parser;

    public static XmlParser getInstance() throws XmlPullParserException {
        if(xmlParser == null) {
            xmlParser = new XmlParser();
        }
        return xmlParser;
    }

    private XmlParser() throws XmlPullParserException {
        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        parser = xmlFactoryObject.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    }

    public List<RssEntity> parse(InputStream inputStream) throws IOException, XmlPullParserException {
        parser.setInput(inputStream, null);
        List<RssEntity> list = parseXml(parser);
        inputStream.close();
        return list;
    }

    private List<RssEntity> parseXml(XmlPullParser parser) {
        int event;
        String text = null;
        List<RssEntity> rssEntities = new ArrayList<RssEntity>();
        RssEntity rssEntity = null;
        try {
            event = parser.getEventType();
            while(event != XmlPullParser.END_DOCUMENT) {

                String name = null;
                switch (event) {
                    case  XmlPullParser.START_TAG:
                        name = parser.getName();
                        if("item".equals(name)) {
                            rssEntity = new RssEntity();
                        } else if(rssEntity != null) {
                            if(TITLE.equals(name)) {
                                rssEntity.setTitle(parser.nextText());
                            } else if(THUMBNAIL.equals(name)) {
                                rssEntity.setUrlThumbnail(parser.getAttributeValue(null, "url"));
                            } else if(DESCRIPTION.equals(name)) {
                                rssEntity.setDescription(parser.nextText());
                            } else if(VIDEO.equals(name)) {
                                rssEntity.setUrlVideo(parser.getAttributeValue(null, "url"));
                            }
                        }
                        break;
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;
                    case XmlPullParser.CDSECT:
                        String cdata = parser.getText();
                        text = cdata.substring(cdata.indexOf("![CDATA["), cdata.indexOf("]]"));
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if("item".equals(name)) {
                            rssEntities.add(rssEntity);
                        }

                        break;
                }

                event = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rssEntities;
    }
}
