package com.tedtalks.rss.fragments;


import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tedtalks.rss.PlayerActivity;
import com.tedtalks.rss.R;
import com.tedtalks.rss.api.RequestHelper;
import com.tedtalks.rss.fragments.adapters.TalksCursorAdapter;
import com.tedtalks.rss.model.RssEntity;
import com.tedtalks.rss.providers.TalksContentProvider;
import com.tedtalks.rss.utils.DemoUtil;

/**
 * Created by user on 9/6/2015.
 */
public class TalksListFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener {

    private TalksCursorAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_talks_list, container, false);
        ListView lvTalks = (ListView) v.findViewById(R.id.lvTalks);
        lvTalks.setAdapter(adapter);
        adapter = new TalksCursorAdapter(getActivity());
        lvTalks.setAdapter(adapter);
        lvTalks.setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getLoaderManager().initLoader(0, null, this);
        RequestHelper.getInstance(getActivity()).getUsers();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Uri uri = Uri.withAppendedPath(TalksContentProvider.URI_TALKS, "offset/" + position);
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if(cursor.moveToFirst()) {
            RssEntity rssEntity = new RssEntity(cursor);
            String urlVideo = rssEntity.getUrlVideo();
            Intent mpdIntent = new Intent(getActivity(), PlayerActivity.class)
                    .setData(Uri.parse(urlVideo))
                    .putExtra(PlayerActivity.CONTENT_TYPE_EXTRA, DemoUtil.TYPE_M4A);
            startActivity(mpdIntent);
        }
        cursor.close();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), TalksContentProvider.URI_TALKS, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

}


