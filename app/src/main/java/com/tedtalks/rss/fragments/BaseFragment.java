package com.tedtalks.rss.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by user on 10/6/2015.
 */
public abstract class BaseFragment extends Fragment {

    protected ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void showProgress() {
        progress.show();
    }



    public void hideProgress() {
        if(progress.isShowing()) {
            progress.dismiss();
        }
    }

    public void showProgressWithMessage(String message) {
        progress.setMessage(message);
        showProgress();
    }

    public void showMessage(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
