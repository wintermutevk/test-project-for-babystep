package com.tedtalks.rss.fragments.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tedtalks.rss.R;
import com.tedtalks.rss.model.RssEntity;


/**
 * Created by user on 9/6/2015.
 */
public class TalksCursorAdapter extends CursorAdapter {

    public TalksCursorAdapter(Context context) {
        super(context, null, false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_list_item, viewGroup, false);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        RssEntity rssEntity = new RssEntity(cursor);
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        if(viewHolder == null) {
            viewHolder = new ViewHolder();
            viewHolder.ivThumbnail = (ImageView) view.findViewById(R.id.ivThumbnail);
            viewHolder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            viewHolder.tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            view.setTag(viewHolder);
        }
        viewHolder.tvTitle.setText(rssEntity.getTitle());
        viewHolder.tvDescription.setText(rssEntity.getDescription());
        Picasso.with(context).load(rssEntity.getUrlThumbnail()).resize(96, 96).centerCrop().into(viewHolder.ivThumbnail);
    }

    private static class ViewHolder {
        public ImageView ivThumbnail;
        public TextView tvTitle;
        public TextView tvDescription;
    }
}
